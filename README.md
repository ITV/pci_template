# Our template for the PCI

Please read the 
[instructions for authors](https://www.combustioninstitute.org/wp-content/uploads/2023/08/Instructions-to-Authors-for-Manuscript-Preparation.40thISOC.pdf) 
and the information in the template.

Run the following command to compile the paper:

```sh
./compile_the_paper.sh
```

Run the following command to remove temporary files:

```sh
 rm paper.aux paper.bbl paper.blg paper.log paper.out paper.pdf paper.spl
```
