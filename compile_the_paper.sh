#!/usr/bin/env bash

pdflatex paper.tex
bibtex paper
pdflatex paper.tex
pdflatex paper.tex
#
#pdftk Sympo_Template_CB.pdf cat 1-9 output Latex2ColFile_Langer.pdf
#pdftk Sympo_Template_CB.pdf cat 10-end output Supplement_Langer.pdf
